from pathway_service.application.domain_model_mapper import CoreModelMapper
from pathway_service.domain.core import PathwayService

class PathwayApplicationContract(object):
    def add_domain_priority(self, domain_priority_pairs): pass
    def add_score_card(self,student_scorecard_tuples): pass
    def create_learning_path_report(self): pass

class PathwayApplication(PathwayApplicationContract):
    
    def __init__(self, pathwayservice):
        if not isinstance(pathwayservice, PathwayService): raise ValueError
        self.pathwayservice = pathwayservice
    def add_domain_priority(self, domain_priority_pairs):
        for domain_priority_pair in domain_priority_pairs:
            builder = CoreModelMapper(domain_priority_tuple=domain_priority_pair)
            domainpriority = builder.build()
            self.pathwayservice.save_domain_priority(domainpriority)
        return 1


    def add_score_card(self,student_scorecard_tuples):
        for student_scorecard_tuple in student_scorecard_tuples:
            builder = CoreModelMapper(student_scorecard_tuple=student_scorecard_tuple)
            student_score_card = builder.build()
            self.pathwayservice.save_score_card(student_score_card)
        return 1

    def create_learning_path_report(self):
        # transactionId = self.pathwayservice.prepare_learning_path()
        (error, paths)  = self.pathwayservice.fetch_learning_path()
        return [self._buildTransientLearningPath(path) for path in paths] if not error else error

    def _buildTransientLearningPath(self, student_learning_path ):
        return ( student_learning_path.student.name, [(str(path.order),str(path.domain.category_code),str(path.grade_level.name)) for path in student_learning_path.learning_path])
        
