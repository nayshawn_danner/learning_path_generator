#!/usr/local/bin/python
from pathway_service.domain.model import *

#[Application Layer Support]
class CoreModelMapper(object):
    
    def __init__(self, **kwargs):
        if len(kwargs)!= 1 : raise ValueError
        model_type, model = list(kwargs.keys())[0], list(kwargs.values())[0]
        builder_factory = {    
                    "domain_priority_tuple": self._buildCCSDomainPriority,
                    "student_scorecard_tuple": self._buildCCSDomainScoreCard
                }

        if (not builder_factory[model_type]): raise ValueError
        self.builder_factory = builder_factory
        self.model_type = model_type
        self.model = model
    
    def build(self):
        
        return self._build_core_model_for_type()

    def _build_core_model_for_type(self):
        model_key = self.model[0]
        model_attributes = self.model[1]
        return self.builder_factory[self.model_type](model_key, model_attributes)

    def _buildCCSDomainScoreCard(self, model_key, model_attributes):
        ccsdomainscore_list = map(lambda domain_level_pair: CCSDomainScore(
                    domain= CCSDomain(category_code=domain_level_pair[0]), 
                    grade_level=GradeLevel(domain_level_pair[1])
                ), model_attributes)
        return CCSDomainScoreCard(student=Student(name=model_key), domain_scores=frozenset(ccsdomainscore_list))
    
    def _buildCCSDomainPriority(self, model_key, model_attributes):
        priority_list = map(lambda domain_level_pair: CCSDomainPriority(
                    priority=Priority(domain_level_pair[0]),
                    domain= CCSDomain(category_code=domain_level_pair[1])
                ), model_attributes)
        return CCSGradeLevelPriority(grade_level=GradeLevel(name=model_key), domain_priorities=frozenset(priority_list))

    