#!/usr/local/bin/python
#[Facade]
from pathway_service.application.pathway import PathwayApplicationContract

class PathwayFacade(object):
    def save_domain_priority_report(self, **kwargs): pass
    def save_student_score_card_report(self, **kwargs): pass
    def get_learning_path_report(self): pass

class CsvPathwayFacade(PathwayFacade):
    
    def __init__(self, pathwayapplication):
        if not isinstance(pathwayapplication, PathwayApplicationContract) :  raise ValueError 
        self.pathwayapplication = pathwayapplication

    def save_domain_priority_report(self, **kwargs):
        domain_priority_report = kwargs["domain_priority_report"]
        
        if  self.pathwayapplication.add_domain_priority(domain_priority_report):
            # print("REPORT_ADDED_SUCCESSFULLY")
            return "REPORT_ADDED_SUCCESSFULLY"
        else:
            # print("ORDER REPORT_ADDED_UNSUCCESSFULLY")
            return "REPORT_ADDED_UNSUCCESSFULLY"

    def save_student_score_card_report(self, **kwargs):
        score_card_report  = kwargs["student_score_card_report"]
        if  self.pathwayapplication.add_score_card(score_card_report):
            # print("TESET SCORE REPORT_ADDED_SUCCESSFULLY")
            return "REPORT_ADDED_SUCCESSFULLY"
        else:
            # print("TESET SCORE REPORT_ADDED_UNSUCCESSFULLY")
            return "REPORT_ADDED_UNSUCCESSFULLY"
    
    def get_learning_path_report(self):
        learning_path_report = self.pathwayapplication.create_learning_path_report()
        return "\n".join([path[0]+","+",".join([ p[2]+"."+p[1] for p in path[1]]) for path in learning_path_report])

class PathwayFacadeFactory(object):
    def __init__(self):
        self._facade_store = {}
    pass

    def put(self,key,facade):
        self._facade_store[key] = facade

    def get(self, key):
        return self._facade_store[key]

