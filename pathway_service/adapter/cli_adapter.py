#!/usr/local/bin/python
#[Adapter]
from pathway_service.facade.pathway import PathwayFacadeFactory

class AbstractCLIAdapter(object):

    def _build_pair_from_csv_line(self, csv_line, schema_support=True,
                                    csv_line_schema=None, key_index=0,
                                    delimiter=","):

        split_csv_line = csv_line.split(delimiter)
        line_schema = csv_line_schema if schema_support else list(range(0,len(split_csv_line)))
        

        if len(split_csv_line) is not len(line_schema): raise ValueError
        key_value_dict = dict(zip(line_schema, split_csv_line))
        key_name = key_value_dict.pop(line_schema[key_index])
        return ((key_name, list(key_value_dict.items())))

    def _build_pairs_from_csv(self, schema_support=True, schema_row=0, **kwargs):
        try:
            csv = kwargs["csv"].strip()
            
            if not isinstance(csv, str) : return (None, "INVALID_REQUEST")
            lines = csv.split("\n")
            if schema_support:
                if len(lines) < 2 : return (None, "INVALID_REQUEST")
                csv_line_schema = lines[schema_row].split(",")
                lines_without_schema_row = lines[1:]

                return ([ self._build_pair_from_csv_line(line, csv_line_schema=csv_line_schema) for line in lines_without_schema_row], None)
            return ([ self._build_pair_from_csv_line(line, schema_support=False) for line in lines], None)

        except ValueError:
            return (None, "INVALID_REQUEST")

class CLIAdapter(AbstractCLIAdapter):
    
    def __init__(self, **kwargs):
        facadefactory = kwargs["facadefactory"]
        if not isinstance(facadefactory, PathwayFacadeFactory): raise ValueError
        self.facadefactory = facadefactory

    def save_domain_priority_report(self, **kwargs):
        try:
            csv_file_ = "".join(kwargs["domain_priority_csv"])
            (pairs_from_csv, error) = self._build_pairs_from_csv(csv=csv_file_, schema_support=False)
            if not error:
                return self.facadefactory.get("CSV_FASCADE").save_domain_priority_report(domain_priority_report=pairs_from_csv)
            # print(error)
            return error
        except Exception:
            pass
            # print("UNEXPECTED EXPLOSION SAVING DOMAIN PRIORITY!!!")
            
    def save_student_score_card_report(self, **kwargs):
        try:
            score_card_csv = "".join(kwargs["score_card_csv"])
            (pairs_from_csv, error) = self._build_pairs_from_csv(csv=score_card_csv)
            if not error:
                return self.facadefactory.get("CSV_FASCADE").save_student_score_card_report(student_score_card_report=pairs_from_csv)
            # print(error)
            return error
        except Exception:
            pass
            # print("UNEXPECTED EXPLOSION SCORE CARD !!!")

    def get_learning_path_report(self):
        return self.facadefactory.get("CSV_FASCADE").get_learning_path_report()
