from pathway_service.domain.core import PathwayService
from pathway_service.domain.model import *

class InMemoryPathwayInfrastructure(PathwayService):
    def __init__(self,inmemory_dict):
        super(InMemoryPathwayInfrastructure,self).__init__()
        if not isinstance(inmemory_dict , dict): raise ValueError
        self.datasource = inmemory_dict

    def add_grade_level_priority(self, grade_level_priority):
        self.datasource["domain_priority"][grade_level_priority.grade_level.name] = [ {"priority":domain_priority.priority.order, "domain":domain_priority.domain.category_code} for domain_priority in grade_level_priority.domain_priorities ]

    def add_score_card(self,score_card):
        self.datasource["score_card"][score_card.student.name] = [ {"domain":score.domain.category_code,"grade_level":score.grade_level.name} for score in score_card.domain_scores]
        

    def get_grade_domain_priorities(self, **kwargs):
        result = self.datasource["domain_priority"][kwargs["grade_level"].name]
        return [CCSDomainPriority( priority=Priority(domain_priority["priority"]), domain=CCSDomain(category_code=domain_priority["domain"])) for domain_priority in result]

    def get_all_score_cards(self):
        result = self.datasource["score_card"]
        return [ CCSDomainScoreCard(student=Student(name=key), \
                             domain_scores=
                                 frozenset([CCSDomainScore(domain=CCSDomain(category_code=domain_grade_dict["domain"]), grade_level=GradeLevel(domain_grade_dict["grade_level"]))
                                            for domain_grade_dict in value                                           
                                           ])) for key, value in result.items()]
