
#!/usr/local/bin/python

"""
NOT SUPPORTED 

class GradeLevelInfructure(object):pass
    # def add_grade_level(self):
    #     pass

class CCSDomainInfructure(object):pass
    # def add_ccsdomain(self):
    #     pass

class StudentInfructure(object):pass
    # def add_student(self):
    #     pass
"""
#[Domain Objects]
class CCSDomain(object):
    def __init__(self, **kwargs):
        category_code = kwargs["category_code"]
        if not (category_code): raise ValueError 
        self.category_code = str(category_code)

    def __key(self):
            return (self.category_code)
    
    def __hash__(self):
        return hash(self.__key())

    def __eq__(x, y):
        return x.__key() == y.__key()

    def __str__(self):
        return str(self.__key())

class GradeLevel(object):
    def __init__(self, name):
        if name == "K" or name == "":
            self.name = "K"
        else:
            self.name = int(name)

    def asNum(self):
        return 0 if self.name is "K" else self.name

    def __key(self):
            return (self.name)
    
    def __hash__(self):
        return hash(self.__key())

    def __eq__(x, y):
        return x.__key() == y.__key()

    def __str__(self):
        return str(self.__key())

class CCSDomainScore(object):
    def __init__(self, **kwargs):
        domain = kwargs["domain"]
        grade_level = kwargs["grade_level"]
        if not isinstance(domain, CCSDomain) : raise ValueError 
        if not isinstance(grade_level, GradeLevel) : raise ValueError 
        self.domain = domain
        self.grade_level = grade_level

    def __key(self):
            return (self.domain, self.grade_level)
    
    def __hash__(self):
        return hash(self.__key())

    def __eq__(x, y):
        return x.__key() == y.__key()

    def __str__(self):
        return str ((self.domain.category_code, self.grade_level.name))

class Student(object):
    def __init__(self, **kwargs):
        self.name = kwargs["name"]

    def __key(self):
            return (self.name)

    def __hash__(self):
        return hash(self.__key())

    def __eq__(x, y):
        return x.__key() == y.__key()

    def __str__(self):
        return str(self.__key())

class CCSDomainScoreCard(object):
    def __init__(self, **kwargs):
        student = kwargs["student"]
        domain_scores = kwargs["domain_scores"]
        if not isinstance(domain_scores, frozenset)     : raise ValueError
        if not isinstance(student, Student)     : raise ValueError
        self.student = student
        self.domain_scores = domain_scores

    def __key(self):
            return (self.student, self.domain_scores)

    def __hash__(self):
        return hash(self.__key())

    def __eq__(x, y):
        return x.__key() == y.__key()

    def __str__(self):
        return str(self.__key())

class Priority(object):
    def __init__(self, order):
        if not isinstance(order, int): raise ValueError
        self.order = order

    def __key(self):
            return (self.order)

    def __hash__(self):
        return hash(self.__key())

    def __eq__(x, y):
        return x.__key() == y.__key()

    def __str__(self):
        return str(self.__key())

class CCSDomainPriority(object):
    def __init__(self, **kwargs):
        self.priority  = kwargs["priority"]
        self.domain  = kwargs["domain"]

    def __key(self):
            return (self.priority, self.domain)

    def __hash__(self):
        return hash(self.__key())

    def __eq__(x, y):
        return x.__key() == y.__key()

    def __str__(self):
        return str(self.__key())

class CCSGradeLevelPriority(object):
    def __init__(self, **kwargs):
        self.domain_priorities  = kwargs["domain_priorities"]
        self.grade_level  = kwargs["grade_level"]

    def __key(self):
            return (self.domain_priorities, self.grade_level)

    def __hash__(self):
        return hash(self.__key())

    def __eq__(x, y):
        return x.__key() == y.__key()

    def __str__(self):
        return str(self.__key())

class TransactionID(object):
    def __init__(self, order):
        if not isinstance(order, int): raise ValueError
        self.order = order

    def __key(self):
            return (self.order)

    def __hash__(self):
        return hash(self.__key())

    def __eq__(x, y):
        return x.__key() == y.__key()

    def __str__(self):
        return str(self.__key())

class StudentLearningPath(object):
    def __init__(self, **kwargs):
        student = kwargs["student"]
        learning_path = kwargs["learning_path"]
        if not isinstance(learning_path, list)     : raise ValueError
        if not isinstance(student, Student)     : raise ValueError
        self.student = student
        self.learning_path = learning_path

    def __key(self):
            return (self.student, frozenset(self.learning_path))

    def __hash__(self):
        return hash(self.__key())

    def __eq__(x, y):
        return x.__key() == y.__key()

    def __str__(self):
        return str(self.__key())

class LearningPath(object):
    def __init__(self, order, **kwargs):
        domain = kwargs["domain"]
        grade_level = kwargs["grade_level"]
        if not isinstance(domain, CCSDomain) :
            raise ValueError
        if not isinstance(grade_level, GradeLevel): raise ValueError 
        if not isinstance(order, int)     : raise ValueError
        self.order = order
        self.domain = domain
        self.grade_level = grade_level

    def __key(self):
            return (self.order, self.domain, self.grade_level)
    
    def __hash__(self):
        return hash(self.__key())

    def __eq__(x, y):
        return x.__key() == y.__key()

    def __str__(self):
        return str ((self.order, self.domain.category_code, self.grade_level.name))

