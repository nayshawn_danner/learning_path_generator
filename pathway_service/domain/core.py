#!/usr/local/bin/python
from pathway_service.domain.model import *
from functools import reduce
#[Domain Support Contracts]
class ScoreCardInfrastructure(object):
    def add_score_card(self, student_score_card):
        pass
    def get_all_score_cards(self):
        pass

class DomainPriorityInfrastructure(object):
    def add_grade_level_priority(self, grade_level_domain_priority):
        pass

    def get_grade_domain_priorities(self, **kwargs):
        pass

#[Domain Core]
class PathwayInfrastructure(DomainPriorityInfrastructure, ScoreCardInfrastructure):
    pass

class PathwayServiceContract(object):
    def save_domain_priority(self, grade_level_domain_priority):pass
    def save_score_card(self,student_score_carsd):pass
    def fetch_learning_path(self, transaction_id=None):pass

class PathwayService(PathwayServiceContract, PathwayInfrastructure):
    def __init__(self):
        self.PATH_LENGTH = 5

    def save_domain_priority(self, grade_level_domain_priority):
        self.add_grade_level_priority(grade_level_domain_priority)
    
    def save_score_card(self,student_score_card):
        self.add_score_card(student_score_card)

    def prepare_learning_path(self): pass

    def fetch_learning_path(self, transaction_id=None):
        student_score_card_list = self.get_all_score_cards()
        return (None, [self._score_card_to_learning_path(student_score_card) for student_score_card in student_score_card_list])
    def _score_card_to_learning_path(self, student_score_card):
        learning_path_list = []
        student_snap_shot = student_score_card
        while len(learning_path_list) != self.PATH_LENGTH:

            weakness=None
            resolveable = False
            weakest_domain = self._getWeakestDomain(student_snap_shot.domain_scores)
            for x in weakest_domain: pass

            if self._hasMultipleWeaknesses(weakest_domain):
                resolveable, weakness = self._resolveWeaknessBasedOnPriority(weakest_domain)
            else:
                weakness = weakest_domain[0]
                priority_dict = self._get_grade_domain_priorities_as_dict(grade_level=weakness.grade_level)
                resolveable = weakness.domain.category_code in priority_dict

            if resolveable:
                step = len(learning_path_list) + 1
                path = LearningPath(step,
                                    domain=weakness.domain,
                                    grade_level=GradeLevel(weakness.grade_level.name))
                learning_path_list.append(path)

            student_snap_shot = self._apply_learning_path(student_snap_shot, weakness)
            
        return StudentLearningPath(student=student_score_card.student, learning_path = learning_path_list)

    def _apply_learning_path(path, student_score_card, domain_score):
        next_domain_grade_level =  CCSDomainScore(domain=domain_score.domain, grade_level=GradeLevel(domain_score.grade_level.asNum() + 1))
        def swap_domain_grade_level(score):
            return score if score.domain != domain_score.domain else next_domain_grade_level
        
        domain_grade_levels = frozenset(map(swap_domain_grade_level, student_score_card.domain_scores))
        return CCSDomainScoreCard(student=student_score_card.student, domain_scores=domain_grade_levels)

    def _getWeakestDomain(self, domain_scores):
        accum = None
        for score in domain_scores:
            if not accum : accum = [score]
            accum_grade = accum[0].grade_level.asNum()
            current_grade=score.grade_level.asNum()
            if current_grade > accum_grade:
                pass
            elif current_grade < accum_grade:
                accum = [score]
            else:
                accum.append(score)
        return accum

    def _hasMultipleWeaknesses(self, score_cards):
        return len(score_cards) > 1

    def _resolveWeaknessBasedOnPriority(self, weakest_domains):
        grade = weakest_domains[0].grade_level
        priority_dict = self._get_grade_domain_priorities_as_dict(grade_level=grade)
        has_priority = lambda domain_score: domain_score.domain.category_code in priority_dict
        is_lower_priority = lambda l_domain, r_domain: priority_dict[l_domain.domain.category_code] < priority_dict[r_domain.domain.category_code]
        def _getWeakest(ldomain, compared_domain):
            if not isinstance(ldomain,tuple): ldomain = (False, ldomain)
            # Make the other the lowest when there is no priority in the grade for a given domain
            if (not has_priority(compared_domain)) and (not has_priority(ldomain[1])):
                return (False, ldomain[1])
            elif (not has_priority(compared_domain)) and has_priority(ldomain[1]):
                return (True, ldomain[1])
            elif (not has_priority(ldomain[1])) and has_priority(compared_domain):
                return (True, compared_domain)
            else:
                return (True, compared_domain if is_lower_priority(compared_domain, ldomain[1]) else ldomain[1])

        return reduce( _getWeakest, weakest_domains);

    def _get_grade_domain_priorities_as_dict(self,grade_level=None):
        ccsdomainpriority = self.get_grade_domain_priorities(grade_level=grade_level)
        return self._convert_domain_priority_dict(ccsdomainpriority)
    
    def _convert_domain_priority_dict(self, ccsdomainpriorities):
        return { domainpriority.domain.category_code : domainpriority.priority.order  for domainpriority in ccsdomainpriorities }
        