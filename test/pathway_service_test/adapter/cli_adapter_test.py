#!/usr/bin/python
#[Adapter]
import unittest
from unittest.mock import MagicMock
from context import pathway_service
from pathway_service.facade.pathway import PathwayFacade, PathwayFacadeFactory
from pathway_service.adapter.cli_adapter import AbstractCLIAdapter, CLIAdapter

class AbstractCLIAdapterTest(unittest.TestCase):
    
    def setUp(self):
        self.abstractcliadapter = AbstractCLIAdapter()
        self.test = {"_1":{
                        "input":"Student Name,RF,RL,RI,L\nAlbin Stanton,2,3,K,3\nErik Purdy,3,1,1,1", 
                        "expected_output": ([
                            ("Albin Stanton", [('RF', '2'), ('RL', '3'), ('RI', 'K'), ('L', '3')]),
                            ("Erik Purdy", [('RF', '3'), ('RL', '1'), ('RI', '1'), ('L', '1')])
                        ],None)},
                    "_2":{
                        "input":"K,RF,RL,RI\n1,RF,RL,RI\n2,RF,RI,RL,L\n3,RF,RL,RI,L\n4,RI,RL,L\n5,RI,RL,L\n6,RI,RL", 
                        "expected_output": (
                                                [("K",[(1,"RF"),(2,"RL"),(3,"RI")]),
                                                ("1",[(1,"RF"),(2,"RL"),(3,"RI")]),
                                                ("2",[(1,"RF"),(2,"RI"),(3,"RL"),(4,"L")]),
                                                ("3",[(1,"RF"),(2,"RL"),(3,"RI"),(4,"L")]),
                                                ("4",[(1,"RI"),(2,"RL"),(3,"L")]),
                                                ("5",[(1,"RI"),(2,"RL"),(3,"L")]),
                                                ("6",[(1,"RI"),(2,"RL")])]
                                            , None)},
                    "_3":{
                        "input":"bad request data", 
                        "expected_output": (None, 'INVALID_REQUEST')},
                    "_4":{
                        "input":"Student Name,RF,RL,RI\nAlbin Stanton,3,K,3\nErik Purdy,3,1", 
                        "expected_output": (None, 'INVALID_REQUEST')},
                    "_5":{
                        "input":"5457", 
                        "expected_output": (None, 'INVALID_REQUEST')},
                    "_6":{
                        "input":"Student Name,RF,RL,RI", 
                        "expected_output": (None, 'INVALID_REQUEST')}

                    }

    def test_build_pairs_from_csv_student_score_data(self):
        actual_domain_priority_pairs, _ = self.abstractcliadapter._build_pairs_from_csv(csv=self.test["_1"]["input"])
        expected_domain_priority_pairs, _ = self.test["_1"]["expected_output"]

        self.assertEqual(len(actual_domain_priority_pairs),len(expected_domain_priority_pairs))
        for i in range(0,len(expected_domain_priority_pairs)):
            self.assertEqual(len(actual_domain_priority_pairs[i][1]),len(expected_domain_priority_pairs[i][1]))
            self.assertEqual(set(actual_domain_priority_pairs[i][1]),set(expected_domain_priority_pairs[i][1]))

    def test_build_pairs_from_csv_for_order_data(self):
        actual_domain_priority_pairs, _ = self.abstractcliadapter._build_pairs_from_csv(csv=self.test["_2"]["input"],schema_support=False)
        expected_domain_priority_pairs, _ = self.test["_2"]["expected_output"]

        self.assertEqual(len(actual_domain_priority_pairs),len(expected_domain_priority_pairs))
        for i in range(0,len(expected_domain_priority_pairs)):
            self.assertEqual(len(actual_domain_priority_pairs[i][1]),len(expected_domain_priority_pairs[i][1]))
            self.assertEqual(set(actual_domain_priority_pairs[i][1]),set(expected_domain_priority_pairs[i][1]))

    def test_build_pairs_from_csv_return_invalid_request_message_when_given_bad_csv_data(self):
        actual_response = self.abstractcliadapter._build_pairs_from_csv(csv=self.test["_3"]["input"])
        self.assertEqual(actual_response, self.test["_3"]["expected_output"])

    def test_build_pairs_from_csv_return_invalid_request_message_when_given_incomplete_request_data(self):
        actual_response = self.abstractcliadapter._build_pairs_from_csv(csv=self.test["_4"]["input"])
        self.assertEqual(actual_response, self.test["_4"]["expected_output"])

    def test_build_pairs_from_csv_return_invalid_request_message_when_given_non_str(self):
        actual_response = self.abstractcliadapter._build_pairs_from_csv(csv=self.test["_5"]["input"])
        self.assertEqual(actual_response, self.test["_5"]["expected_output"])

    def test_build_pairs_from_csv_return_invalid_request_message_when_given_less_than_two_lines(self):
        actual_response = self.abstractcliadapter._build_pairs_from_csv(csv=self.test["_6"]["input"])
        self.assertEqual(actual_response, self.test["_6"]["expected_output"])

class CLIAdapterTest(unittest.TestCase):
    
    def setUp(self):

        self.facadefactory = PathwayFacadeFactory()
# <<<<<<< d3a174e282a97ffe9e779a223864d47cb45cdc4c
        self.facadefactory.put("CSV_FASCADE", PathwayFacade())
# =======
#         self.facadefactory.put("CLI_FASCADE", PathwayFacade())
# >>>>>>> learning path complete up to domain
        self.cliadapter = CLIAdapter(facadefactory=self.facadefactory)
        self.test = {"_1":{
                        "input":(("SucessfullRequest",1), None), 
                        "expected_output": ("SucessfullRequest",1)},
                    "_2":{
                        "input":"Student Name,RF,RL,RI,L\nAlbin Stanton,2,3,K,3\nErik Purdy,3,1,1,1", 
                        "expected_output": [
                            ("Albin Stanton", [('RF', '2'), ('RL', '3'), ('RI', 'K'), ('L', '3')]),
                            ("Erik Purdy", [('RF', '3'), ('RL', '1'), ('RI', '1'), ('L', '1')])
                        ]},
                    "_3":{
                        "input":"bad request data", 
                        "expected_output": "INVALID_REQUEST"},
                    "_4":{
                        "input":"Student Name,RF,RL,RI\nAlbin Stanton,3,K,3\nErik Purdy,3,1", 
                        "expected_output": "INVALID_REQUEST"}
        }

    def test_has_facadefactory_after_init(self):
        self.assertIsInstance(self.cliadapter.facadefactory, PathwayFacadeFactory)

    def test_init_raises_without_facadefactory(self):
        self.assertRaises(Exception, lambda _: CLIAdapter(facadefactory=None), None)

    def test_save_domain_priority_report_fowards_transfromed_csv_to_facade(self):
        self.cliadapter._build_pairs_from_csv = MagicMock(return_value=self.test["_1"]["input"])
# <<<<<<< d3a174e282a97ffe9e779a223864d47cb45cdc4c
        self.facadefactory.get("CSV_FASCADE").save_domain_priority_report = MagicMock(return_value=1)
        self.cliadapter.save_domain_priority_report(domain_priority_csv="any,csv,would,do")
        self.facadefactory.get("CSV_FASCADE").save_domain_priority_report.assert_called_with(domain_priority_report=self.test["_1"]["expected_output"])

    def test_save_score_card_report_fowards_transfromed_csv_to_facade(self):
        self.cliadapter._build_pairs_from_csv = MagicMock(return_value=self.test["_1"]["input"])
        self.facadefactory.get("CSV_FASCADE").save_student_score_card_report = MagicMock(return_value=1)
        self.cliadapter.save_student_score_card_report(score_card_csv="any,csv,would,do")
        self.facadefactory.get("CSV_FASCADE").save_student_score_card_report.assert_called_with(student_score_card_report=self.test["_1"]["expected_output"])
    
    def test_get_learning_path_report_calls_facade(self):
        self.facadefactory.get("CSV_FASCADE").get_learning_path_report = MagicMock(return_value=1)
        self.cliadapter.get_learning_path_report()
        self.facadefactory.get("CSV_FASCADE").get_learning_path_report.assert_called_with()
# =======
#         self.facadefactory.get("CLI_FASCADE").save_domain_priority_report = MagicMock(return_value=1)
#         self.cliadapter.save_domain_priority_report(csv="any,csv,would,do")
#         self.facadefactory.get("CLI_FASCADE").save_domain_priority_report.assert_called_with(domain_prioritity_report=self.test["_1"]["expected_output"])

#     def test_save_score_card_report_fowards_transfromed_csv_to_facade(self):
#         self.cliadapter._build_pairs_from_csv = MagicMock(return_value=self.test["_1"]["input"])
#         self.facadefactory.get("CLI_FASCADE").save_student_score_card_report = MagicMock(return_value=1)
#         self.cliadapter.save_student_score_card_report(csv="any,csv,would,do")
#         self.facadefactory.get("CLI_FASCADE").save_student_score_card_report.assert_called_with(student_score_card_report=self.test["_1"]["expected_output"])
    
#     def test_get_learning_path_report_calls_facade(self):
#         self.facadefactory.get("CLI_FASCADE").get_learning_path_report = MagicMock(return_value=1)
#         self.cliadapter.get_learning_path_report()
#         self.facadefactory.get("CLI_FASCADE").get_learning_path_report.assert_called_with()
# >>>>>>> learning path complete up to domain

if __name__ == "__main__":
    unittest.main() 
