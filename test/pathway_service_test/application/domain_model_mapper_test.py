#!/usr/bin/python
import unittest
from unittest.mock import MagicMock
from context import pathway_service
from pathway_service.domain.model import *
from pathway_service.application.domain_model_mapper import CoreModelMapper

class CoreModelMapperTest(unittest.TestCase):
    def setUp(self):
        self.test = {
                        "_1": {"input": ("shawn", [('RF', '2'), ('L', '3'), ('RI', 'K'), ('RL', '3')]),
                            "expected_output":CCSDomainScoreCard(student=Student(name="shawn"), domain_scores=frozenset([ 
                                CCSDomainScore(domain=CCSDomain(category_code="RF"), grade_level=GradeLevel("2")),
                                CCSDomainScore(domain=CCSDomain(category_code="RL"), grade_level=GradeLevel("3")),
                                CCSDomainScore(domain=CCSDomain(category_code="RI"), grade_level=GradeLevel("K")),
                                CCSDomainScore(domain=CCSDomain(category_code="L"), grade_level=GradeLevel("3"))
                                ]))
                            },
                        "_2": {"input": ('john', [('RF', ''), ('L', ''), ('RI', 'K'), ('RL', '')]),
                                "expected_output":CCSDomainScoreCard(student=Student(name="john"), domain_scores=frozenset([ 
                                CCSDomainScore(domain=CCSDomain(category_code="RF"), grade_level=GradeLevel("")),
                                CCSDomainScore(domain=CCSDomain(category_code="RL"), grade_level=GradeLevel("")),
                                CCSDomainScore(domain=CCSDomain(category_code="RI"), grade_level=GradeLevel("K")),
                                CCSDomainScore(domain=CCSDomain(category_code="L"), grade_level=GradeLevel(""))
                                ]))
                            }}

    def test_build_from_line_returns_ScoreCard_when_request_is_valid(self): 
        actual_score_set = CoreModelMapper(student_scorecard_tuple=self.test["_1"]["input"]).build()
        self.assertEqual(actual_score_set ,self.test["_1"]["expected_output"])


    def test_build_from_line_returns_ScoreCard_when_request_is_valid_without_scores(self):
        actual_score_set = CoreModelMapper(student_scorecard_tuple=self.test["_2"]["input"]).build()
        self.assertEqual(actual_score_set ,self.test["_2"]["expected_output"])

    def test_build_from_line_returns_DomainPriority_when_request_is_valid(self): 
        actual_score_set = CoreModelMapper(student_scorecard_tuple=self.test["_1"]["input"]).build()
        self.assertEqual(actual_score_set ,self.test["_1"]["expected_output"])

    
if __name__ == "__main__":
    unittest.main() 