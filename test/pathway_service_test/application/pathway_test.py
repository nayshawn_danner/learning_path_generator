#!/usr/bin/python
#[Application]
import unittest
from unittest.mock import MagicMock
from context import pathway_service
from pathway_service.domain.core import PathwayService
from pathway_service.domain.model import *
from pathway_service.application.pathway import PathwayApplication

class PathwayApplicationTest(unittest.TestCase):
    
    def setUp(self):
        self.pathwayservice = PathwayService()
        self.pathwayapplication = PathwayApplication(self.pathwayservice)
        self.test = {
                        "_1":{
                            "input":[("K",[(1,"RF"),(2,"RL"),(3,"RI")]),
                                                ("1",[(1,"RF"),(2,"RL"),(3,"RI")]),
                                                ("2",[(1,"RF"),(2,"RI"),(3,"RL"),(4,"L")]),
                                                ("3",[(1,"RF"),(2,"RL"),(3,"RI"),(4,"L")]),
                                                ("4",[(1,"RI"),(2,"RL"),(3,"L")]),
                                                ("5",[(1,"RI"),(2,"RL"),(3,"L")]),
                                                ("6",[(1,"RI"),(2,"RL")])], 
                            "expected_output": 7},
                        "_2":{
                            "input":[
                                ("Albin Stanton", [('RF', '2'), ('RL', '3'), ('RI', 'K'), ('L', '3')]),
                                ("Erik Purdy", [('RF', '3'), ('RL', '1'), ('RI', '1'), ('L', '1')])
                            ], 
                            "expected_output": 2
                        },
                        "_5":{
                            "input":[TransactionID(3234324), (ValueError("Service Unavailable"), None)]
                            ,"expected_output": ValueError("Service Unavailable") },
                        "_7": {"input":StudentLearningPath(student=Student(name="Albin Stanton"), learning_path=[
                                            LearningPath(1,domain=CCSDomain(category_code="RF"), grade_level=GradeLevel("2")),
                                            LearningPath(2,domain=CCSDomain(category_code="RL"), grade_level=GradeLevel("3")),
                                            LearningPath(3,domain=CCSDomain(category_code="RL"), grade_level=GradeLevel("4")),
                                            LearningPath(4,domain=CCSDomain(category_code="RF"), grade_level=GradeLevel("5")),
                                            LearningPath(5,domain=CCSDomain(category_code="L"), grade_level=GradeLevel("6"))
                                            ]), 
                                "expected_output":
                                ("Albin Stanton", [("1","RF","2"),("2","RL","3"),("3","RL","4"),("4","RF","5"),("5","L","6")] )}
                        

                    }

    def test_init_raises_if_not_given_PathwayService(self):
            self.assertRaises(ValueError, lambda _ : PathwayApplication("not a pathway service"), None)

    def test_add_domain_priority_saves_for_all_grade_level_domain_priorities(self):
        self.pathwayservice.save_domain_priority = MagicMock(return_value=1)
        self.pathwayapplication.add_domain_priority(self.test["_1"]["input"])
        service_call_count = self.pathwayservice.save_domain_priority.call_count

        self.assertEqual(service_call_count, self.test["_1"]["expected_output"])

    def test_add_score_submit_saves_for_all_student_score_cards(self):
        self.pathwayservice.save_score_card = MagicMock(return_value=1)
        self.pathwayapplication.add_score_card(self.test["_2"]["input"])
        service_call_count = self.pathwayservice.save_score_card.call_count
        self.assertEqual(service_call_count, self.test["_2"]["expected_output"])

    def test_create_learning_path_returns_error_when_processing_fails(self):
        self.pathwayservice.prepare_learning_path = MagicMock(return_value=self.test["_5"]["input"][0])
        self.pathwayservice.fetch_learning_path = MagicMock(return_value=self.test["_5"]["input"][1])
        actual_response = self.pathwayapplication.create_learning_path_report()
        self.assertEqual(str(actual_response), str(self.test["_5"]["expected_output"]))

    def skip_test_create_learning_path_calls_fetch_learning_path_with_transactionId_from_prepare(self):
        self.pathwayservice.prepare_learning_path = MagicMock(return_value=self.test["_3"]["input"][0])
        self.pathwayservice.fetch_learning_path = MagicMock(return_value=self.test["_3"]["input"][1])
        self.pathwayapplication.create_learning_path_report()
        self.pathwayservice.fetch_learning_path.assert_called_with(self.test["_3"]["expected_output"])

    def test__buildTransientLearningPath_return_correct_structure(self):
        actual_score_set = self.pathwayapplication._buildTransientLearningPath(self.test["_7"]["input"])
        self.assertEqual(actual_score_set ,self.test["_7"]["expected_output"])
if __name__ == "__main__":
    unittest.main() 
