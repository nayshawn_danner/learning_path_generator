#!/usr/bin/python
#[Facade]
import unittest
from unittest.mock import MagicMock
from context import pathway_service
from pathway_service.application.pathway import *
from pathway_service.facade.pathway import *
class CsvPathwayFacadeTest(unittest.TestCase):
    
    def setUp(self):
        self.pathwayapplication = PathwayApplicationContract()
        self.csvpathwayfacade = CsvPathwayFacade(self.pathwayapplication)
        self.test = {
                    "_1":{
                        "input":1, 
                        "expected_output": "REPORT_ADDED_SUCCESSFULLY"},
                    "_2":{
                        "input":0, 
                        "expected_output": "REPORT_ADDED_UNSUCCESSFULLY"}
                    ,
                    "_3":{
                        "input":[("Albin Stanton", [("1","RI","K"),("2","RI","1"),("3","RF","2"),("4","RI","2"),("5","RF","3")] )], 
                        "expected_output":"Albin Stanton,K.RI,1.RI,2.RF,2.RI,3.RF"}
                    }

    def test_save_domain_priority_report_return_ok_when_given_good_request_data(self):
        self.pathwayapplication.add_domain_priority = MagicMock(return_value=self.test["_1"]["input"])
        actual_response = self.csvpathwayfacade.save_domain_priority_report(domain_priority_report=[("",[])])

        self.assertEqual(actual_response, self.test["_1"]["expected_output"])

    def test_save_domain_priority_report_return_a_process_failed_msg_when_application_fails(self):
        self.pathwayapplication.add_domain_priority = MagicMock(return_value=self.test["_2"]["input"])
        actual_response = self.csvpathwayfacade.save_domain_priority_report(domain_priority_report=[("",[])])

        self.assertEqual(actual_response, self.test["_2"]["expected_output"])

    def test_save_student_score_card_report_return_ok_when_given_good_request_data(self):
        self.pathwayapplication.add_score_card = MagicMock(return_value=self.test["_1"]["input"])
        actual_response = self.csvpathwayfacade.save_student_score_card_report(student_score_card_report=[("",[])])
        self.assertEqual(actual_response, self.test["_1"]["expected_output"])

    def test_save_student_score_card_report_return_a_process_failed_msg_when_application_fails(self):
        self.pathwayapplication.add_score_card = MagicMock(return_value=self.test["_2"]["input"])
        actual_response = self.csvpathwayfacade.save_student_score_card_report(student_score_card_report=[("",[])])

        self.assertEqual(actual_response, self.test["_2"]["expected_output"])

    def test_get_learning_path_calls_applications_get_learning_path(self):
        self.pathwayapplication.create_learning_path_report = MagicMock()
        self.csvpathwayfacade.get_learning_path_report()

        self.pathwayapplication.create_learning_path_report.assert_called_with()
    
    def test__get_learning_path_report_returns_csv_file_response(self):
        self.pathwayapplication.create_learning_path_report = MagicMock(return_value=self.test["_3"]["input"])
        actual_response = self.csvpathwayfacade.get_learning_path_report()

        self.assertEqual(actual_response, self.test["_3"]["expected_output"])

class PathwayFacadeFactoryTest(unittest.TestCase):
    def setUp(self):
        self.factory = PathwayFacadeFactory()

    def test_put_excepts_PathwayFacade(self):
        facade = PathwayFacade()
        self.factory.put("facade",facade)
        self.assertIsInstance(self.factory._facade_store["facade"], PathwayFacade)

    def test_put_only_excepts_PathwayFacade(self):
        self.assertRaises(Exception,lambda _: self.factory.put(1) ,None)

    def test_get_return_PathwayFacade(self):
        facade = PathwayFacade()
        self.factory.put("facade",facade)
        self.assertIsInstance(self.factory.get("facade"), PathwayFacade)

if __name__ == "__main__":
    unittest.main() 
