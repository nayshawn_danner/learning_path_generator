import unittest
from unittest.mock import MagicMock
from context import pathway_service
from pathway_service.domain.model import *
from pathway_service.domain.infrastructure.in_memory import InMemoryPathwayInfrastructure

class InMemoryPathwayInfrastructureTest(unittest.TestCase):
    def setUp(self):
        self.dict_datasource = {"score_card":{}, "domain_priority":{}}
        self.infrastructure = InMemoryPathwayInfrastructure(self.dict_datasource)
        self.test = {"_1":{
                            "input": CCSGradeLevelPriority(grade_level=GradeLevel(name="3"), domain_priorities=frozenset([
                                    CCSDomainPriority(priority=Priority(1), domain= CCSDomain(category_code="RF")),
                                    CCSDomainPriority(priority=Priority(2), domain= CCSDomain(category_code="RL")),
                                    CCSDomainPriority(priority=Priority(3), domain= CCSDomain(category_code="RI")),
                                    
                                ])),

                            "expected_output":{"domain_priority": {3:[{"priority":1,"domain":"RF"},
                                                    {"priority":2,"domain":"RL"},
                                                    {"priority":3,"domain":"RI"}]}}
                        },
                        "_2":{
                            "input": [{3:[{"priority":1,"domain":"RF"},
                                                    {"priority":2,"domain":"RL"},
                                                    {"priority":3,"domain":"RI"}]},
                                        GradeLevel(3)],

                            "expected_output":[
                                    CCSDomainPriority(priority=Priority(1), domain= CCSDomain(category_code="RF")),
                                    CCSDomainPriority(priority=Priority(2), domain= CCSDomain(category_code="RL")),
                                    CCSDomainPriority(priority=Priority(3), domain= CCSDomain(category_code="RI")),
                                    
                                ],
                        }
                    }
    def test_init_with_dict_datasource(self):
        self.assertIsInstance(self.infrastructure.datasource, dict)

    def test_init_fails_when_datasource_is_not_dict(self):
        self.assertRaises(ValueError,lambda _: InMemoryPathwayInfrastructure(""), None)

    def test_add_grade_level_priority_successfully_stores_score_card(self):
        self.infrastructure.add_grade_level_priority(self.test["_1"]["input"])

        for grade_level in self.test["_1"]["expected_output"]["domain_priority"]:
            actual_domain_priority_list = self.dict_datasource["domain_priority"][grade_level]
            expected_domain_priority_list = self.test["_1"]["expected_output"]["domain_priority"][grade_level]

            def flatten_score_dict(score_dict): 
                priority, domain = score_dict.items()
                return (priority, domain)

            self.assertEqual(
                                set(map(flatten_score_dict, actual_domain_priority_list)), 
                                set(map(flatten_score_dict, expected_domain_priority_list))
                             )

    def test_get_grade_domain_priorities(self):
        self.dict_datasource["domain_priority"] = self.test["_2"]["input"][0]
        actual_ccsdomainpriority = self.infrastructure.get_grade_domain_priorities(grade_level=self.test["_2"]["input"][1])
        self.assertEqual(actual_ccsdomainpriority, self.test["_2"]["expected_output"])

class InMemoryPathwayInfrastructure_Test(unittest.TestCase):
    def setUp(self):
        self.dict_datasource = {"score_card":{}, "domain_priority":{}}
        self.infrastructure = InMemoryPathwayInfrastructure(self.dict_datasource)
        self.test = {"_1":{
                            "input":CCSDomainScoreCard(student=Student(name="shawn"), domain_scores=frozenset([ 
                            CCSDomainScore(domain=CCSDomain(category_code="RF"), grade_level=GradeLevel("2")),
                            CCSDomainScore(domain=CCSDomain(category_code="RL"), grade_level=GradeLevel("3")),
                            CCSDomainScore(domain=CCSDomain(category_code="RI"), grade_level=GradeLevel("K")),
                            CCSDomainScore(domain=CCSDomain(category_code="L"), grade_level=GradeLevel("3"))
                            ])),
                            "expected_output":{"score_card":{"shawn":[{"domain":"RF","grade_level":2},
                                                        {"domain":"RL","grade_level":3},
                                                        {"domain":"RI","grade_level":"K"},
                                                        {"domain":"L","grade_level":3}]}}
                        },
                        "_2":{
                            "input":{"shawn":[{"domain":"RF","grade_level":2},
                                                        {"domain":"RL","grade_level":3},
                                                        {"domain":"RI","grade_level":"K"},
                                                        {"domain":"L","grade_level":3}]},

                            "expected_output":[CCSDomainScoreCard(student=Student(name="shawn"), domain_scores=frozenset([ 
                            CCSDomainScore(domain=CCSDomain(category_code="RF"), grade_level=GradeLevel("2")),
                            CCSDomainScore(domain=CCSDomain(category_code="RL"), grade_level=GradeLevel("3")),
                            CCSDomainScore(domain=CCSDomain(category_code="RI"), grade_level=GradeLevel("K")),
                            CCSDomainScore(domain=CCSDomain(category_code="L"), grade_level=GradeLevel("3"))
                            ]))]
                        }
                    }
    def test_init_with_dict_datasource(self):
        self.assertIsInstance(self.infrastructure.datasource, dict)

    def test_init_fails_when_datasource_is_not_dict(self):
        self.assertRaises(ValueError,lambda _: InMemoryPathwayInfrastructure(""), None)

    def test_get_score_card_returns_CCSDomainScorecard(self):
            self.dict_datasource["score_card"] = self.test["_2"]["input"]
            self.assertEqual(self.infrastructure.get_all_score_cards(), self.test["_2"]["expected_output"] )

    def test_add_score_card_successfully_stores_score_card(self):
        self.infrastructure.add_score_card(self.test["_1"]["input"])

        for expected_name in self.test["_1"]["expected_output"]["score_card"]:
            actual_score_list = self.dict_datasource["score_card"][expected_name]
            expected_score_list = self.test["_1"]["expected_output"]["score_card"][expected_name]

            def flatten_score_dict(score_dict): 
                domain_pairs, grade_level_pairs = score_dict.items()
                return (domain_pairs, grade_level_pairs)

            self.assertEqual(
                                set(map(flatten_score_dict, actual_score_list)), 
                                set(map(flatten_score_dict, expected_score_list))
                             )

if __name__ == "__main__":
    unittest.main() 
