#!/usr/bin/python

import unittest
from unittest.mock import MagicMock
from context import pathway_service
from pathway_service.domain.model import *
from pathway_service.domain.core import *

class PathwayServiceTest(unittest.TestCase):
    
    def setUp(self):
        self.pathwayservice = PathwayService()
        self.test = {
                        "_1": {
                                "input": CCSDomainScoreCard(student=Student(name="shawn"), domain_scores=frozenset([
                                            CCSDomainScore(domain=CCSDomain(category_code="RF"), grade_level=GradeLevel("2")),
                                            CCSDomainScore(domain=CCSDomain(category_code="RL"), grade_level=GradeLevel("3")),
                                            CCSDomainScore(domain=CCSDomain(category_code="RI"), grade_level=GradeLevel("K")),
                                            CCSDomainScore(domain=CCSDomain(category_code="L"), grade_level=GradeLevel("3"))
                                            ])),
                                "expected_output": CCSDomainScoreCard(student=Student(name="shawn"), domain_scores=frozenset([
                                            CCSDomainScore(domain=CCSDomain(category_code="RF"), grade_level=GradeLevel("2")),
                                            CCSDomainScore(domain=CCSDomain(category_code="RL"), grade_level=GradeLevel("3")),
                                            CCSDomainScore(domain=CCSDomain(category_code="RI"), grade_level=GradeLevel("K")),
                                            CCSDomainScore(domain=CCSDomain(category_code="L"), grade_level=GradeLevel("3"))
                                            ]))
                                },
                        "_2": {
                                "input": CCSGradeLevelPriority(grade_level=GradeLevel(name="3"), domain_priorities=frozenset([
                                    CCSDomainPriority(priority=Priority(1), domain= CCSDomain(category_code="RF")),
                                    CCSDomainPriority(priority=Priority(2), domain= CCSDomain(category_code="RL")),
                                    CCSDomainPriority(priority=Priority(3), domain= CCSDomain(category_code="RI")),
                                    
                                ])),
                                "expected_output": CCSGradeLevelPriority(grade_level=GradeLevel(name="3"), domain_priorities=frozenset([
                                    CCSDomainPriority(priority=Priority(1), domain= CCSDomain(category_code="RF")),
                                    CCSDomainPriority(priority=Priority(2), domain= CCSDomain(category_code="RL")),
                                    CCSDomainPriority(priority=Priority(3), domain= CCSDomain(category_code="RI")),
                                    
                                ]))
                            },
                        "_3": {
                                "input": [CCSDomainScoreCard(student=Student(name="Albin Stanton"), domain_scores=frozenset([
                                            CCSDomainScore(domain=CCSDomain(category_code="RF"), grade_level=GradeLevel("2"))]))
                                          ,[
                                                CCSDomainPriority(priority=Priority(1), domain= CCSDomain(category_code="RF")),
                                                CCSDomainPriority(priority=Priority(2), domain= CCSDomain(category_code="RI")),
                                                CCSDomainPriority(priority=Priority(3), domain= CCSDomain(category_code="RL")),
                                                CCSDomainPriority(priority=Priority(4), domain= CCSDomain(category_code="L"))
                                            ]
    
                                ],
                                "expected_output": StudentLearningPath(student=Student(name="Albin Stanton"), learning_path=[
                                            LearningPath(1,domain=CCSDomain(category_code="RF"), grade_level=GradeLevel("2")),
                                            LearningPath(2,domain=CCSDomain(category_code="RF"), grade_level=GradeLevel("3")),
                                            LearningPath(3,domain=CCSDomain(category_code="RF"), grade_level=GradeLevel("4")),
                                            LearningPath(4,domain=CCSDomain(category_code="RF"), grade_level=GradeLevel("5")),
                                            LearningPath(5,domain=CCSDomain(category_code="RF"), grade_level=GradeLevel("6"))
                                            ])
                            },
                            "_4": {
                                "input": frozenset([
                                            CCSDomainScore(domain=CCSDomain(category_code="RF"), grade_level=GradeLevel("2")),
                                            CCSDomainScore(domain=CCSDomain(category_code="RL"), grade_level=GradeLevel("3")),
                                            CCSDomainScore(domain=CCSDomain(category_code="RI"), grade_level=GradeLevel("K")),
                                            CCSDomainScore(domain=CCSDomain(category_code="L"), grade_level=GradeLevel("3"))
                                            ]),
                                "expected_output": CCSDomainScore(domain=CCSDomain(category_code="RI"), grade_level=GradeLevel("K")),                    
                            },
                            "_5": {
                                "input": [CCSDomainScore(domain=CCSDomain(category_code="RI"), grade_level=GradeLevel("K")),
                                          CCSDomainScore(domain=CCSDomain(category_code="RL"), grade_level=GradeLevel("K"))],
                                "expected_output": [CCSDomainScore(domain=CCSDomain(category_code="RI"), grade_level=GradeLevel("K")),
                                                    CCSDomainScore(domain=CCSDomain(category_code="RL"), grade_level=GradeLevel("K"))]
                            },
                            "_6": {
                                "input": [
                                            CCSDomainPriority(priority=Priority(1), domain= CCSDomain(category_code="RF")),
                                            CCSDomainPriority(priority=Priority(2), domain= CCSDomain(category_code="RL")),
                                            CCSDomainPriority(priority=Priority(3), domain= CCSDomain(category_code="RI")),
                                         ],
                                "expected_output":{"RF":1,"RL":2,"RI":3},            
                            },
                            "_7":{
                                "input":[ [
                                            CCSDomainPriority(priority=Priority(1), domain= CCSDomain(category_code="RF")),
                                            CCSDomainPriority(priority=Priority(2), domain= CCSDomain(category_code="RL")),
                                            CCSDomainPriority(priority=Priority(3), domain= CCSDomain(category_code="RI")),
                                         ],[CCSDomainScore(domain=CCSDomain(category_code="RI"), grade_level=GradeLevel("K")),
                                          CCSDomainScore(domain=CCSDomain(category_code="RL"), grade_level=GradeLevel("K"))]] ,
                                "expected_output":CCSDomainScore(domain=CCSDomain(category_code="RL"), grade_level=GradeLevel("K"))
                            },
                            "_8":{
                                "input":[[CCSGradeLevelPriority(grade_level=GradeLevel(name="K"), domain_priorities=frozenset([
                                                    CCSDomainPriority(priority=Priority(1), domain= CCSDomain(category_code="RF")),
                                                    CCSDomainPriority(priority=Priority(2), domain= CCSDomain(category_code="RL")),
                                                    CCSDomainPriority(priority=Priority(3), domain= CCSDomain(category_code="RI")),
                                                    
                                                ])),
                                                CCSGradeLevelPriority(grade_level=GradeLevel(name="1"), domain_priorities=frozenset([
                                                    CCSDomainPriority(priority=Priority(1), domain= CCSDomain(category_code="RF")),
                                                    CCSDomainPriority(priority=Priority(2), domain= CCSDomain(category_code="RL")),
                                                    CCSDomainPriority(priority=Priority(3), domain= CCSDomain(category_code="RI")),
                                                ]))],GradeLevel("1")
                                                    ,CCSDomainScore(domain=CCSDomain(category_code="RF"), grade_level=GradeLevel("1")),
                                                    CCSDomainScore(domain=CCSDomain(category_code="RL"), grade_level=GradeLevel("1"))] ,
                                "expected_output":CCSDomainScore(domain=CCSDomain(category_code="RF"), grade_level=GradeLevel("2"))
                            },"_9": {
                                        "input": [
                                                    [
                                                        CCSDomainScoreCard(student=Student(name="Eric Purdy"), domain_scores=frozenset([
                                                        CCSDomainScore(domain=CCSDomain(category_code="RF"), grade_level=GradeLevel("3")),
                                                        CCSDomainScore(domain=CCSDomain(category_code="RL"), grade_level=GradeLevel("1")),
                                                        CCSDomainScore(domain=CCSDomain(category_code="RI"), grade_level=GradeLevel("1")),
                                                        CCSDomainScore(domain=CCSDomain(category_code="L"), grade_level=GradeLevel("1"))
                                                        ]))
                                                        ]
                                                    ,{
                                                        "k":[
                                                                CCSDomainPriority(priority=Priority(1), domain= CCSDomain(category_code="RF")),
                                                                CCSDomainPriority(priority=Priority(2), domain= CCSDomain(category_code="RL")),
                                                                CCSDomainPriority(priority=Priority(3), domain= CCSDomain(category_code="RI"))],
                                                        "1":[
                                                                CCSDomainPriority(priority=Priority(1), domain= CCSDomain(category_code="RF")),
                                                                CCSDomainPriority(priority=Priority(2), domain= CCSDomain(category_code="RL")),
                                                                CCSDomainPriority(priority=Priority(3), domain= CCSDomain(category_code="RI"))],
                                                        "2":[
                                                                CCSDomainPriority(priority=Priority(1), domain= CCSDomain(category_code="RF")),
                                                                CCSDomainPriority(priority=Priority(2), domain= CCSDomain(category_code="RI")),
                                                                CCSDomainPriority(priority=Priority(3), domain= CCSDomain(category_code="RL")),
                                                                CCSDomainPriority(priority=Priority(4), domain= CCSDomain(category_code="L"))],
                                                        "3":[
                                                                CCSDomainPriority(priority=Priority(1), domain= CCSDomain(category_code="RF")),
                                                                CCSDomainPriority(priority=Priority(2), domain= CCSDomain(category_code="RL")),
                                                                CCSDomainPriority(priority=Priority(3), domain= CCSDomain(category_code="RI")),
                                                                CCSDomainPriority(priority=Priority(4), domain= CCSDomain(category_code="L"))],
                                                        "4":[
                                                                CCSDomainPriority(priority=Priority(1), domain= CCSDomain(category_code="RI")),
                                                                CCSDomainPriority(priority=Priority(2), domain= CCSDomain(category_code="RL")),
                                                                CCSDomainPriority(priority=Priority(3), domain= CCSDomain(category_code="L"))],
                                                        "5":[
                                                                CCSDomainPriority(priority=Priority(1), domain= CCSDomain(category_code="RI")),
                                                                CCSDomainPriority(priority=Priority(2), domain= CCSDomain(category_code="RL")),
                                                                CCSDomainPriority(priority=Priority(3), domain= CCSDomain(category_code="L"))],
                                                        "6":[
                                                                CCSDomainPriority(priority=Priority(1), domain= CCSDomain(category_code="RI")),
                                                                CCSDomainPriority(priority=Priority(2), domain= CCSDomain(category_code="RL"))], 

                                                        }
                                                    
                                                    ],
                                        "expected_output":(None,[StudentLearningPath(student=Student(name="Eric Purdy"), learning_path=[
                                            LearningPath(1,domain=CCSDomain(category_code="RL"), grade_level=GradeLevel("1")),
                                            LearningPath(2,domain=CCSDomain(category_code="RI"), grade_level=GradeLevel("1")),
                                            LearningPath(3,domain=CCSDomain(category_code="RI"), grade_level=GradeLevel("2")),
                                            LearningPath(4,domain=CCSDomain(category_code="RL"), grade_level=GradeLevel("2")),
                                            LearningPath(5,domain=CCSDomain(category_code="L"), grade_level=GradeLevel("2"))
                                            ])])
                                    }

                            }
    def test_init_creates_PathwayService_as_ScoreCardInfructure(self):
        self.assertIsInstance(self.pathwayservice, ScoreCardInfrastructure)

    def test_save_score_card_attempts_saves_score_card(self):
            self.pathwayservice.add_score_card = MagicMock(return_value=1)
            self.pathwayservice.save_score_card(self.test["_1"]["input"])
            self.pathwayservice.add_score_card.assert_called_with(self.test["_1"]["expected_output"])

    def test_init_creates_PathwayService_as_DomainPriorityInfrastructure(self):
        self.assertIsInstance(self.pathwayservice, DomainPriorityInfrastructure)

    def test_save_score_card_attempts_saves_score_card(self):
            self.pathwayservice.add_grade_level_priority = MagicMock(return_value=1)
            self.pathwayservice.save_domain_priority(self.test["_2"]["input"])
            self.pathwayservice.add_grade_level_priority.assert_called_with(self.test["_2"]["expected_output"])

    def test_map_score_card_to_learning_path(self):
        self.pathwayservice.get_grade_domain_priorities = MagicMock(return_value=self.test["_3"]["input"][1])
        learning_path = self.pathwayservice._score_card_to_learning_path(self.test["_3"]["input"][0])
        self.assertEqual(learning_path, self.test["_3"]["expected_output"])
    
    def test_getWeakestDomain(self):
        actual = self.pathwayservice._getWeakestDomain(self.test["_4"]["input"])
        self.assertEqual(actual[0], self.test["_4"]["expected_output"] )

    def test_getWeakestDomain_all_when_is_a_tie(self):
        actual = self.pathwayservice._getWeakestDomain(self.test["_5"]["input"])
        self.assertEqual(frozenset(actual), frozenset(self.test["_5"]["expected_output"]) )

    def test_convert_domain_priority_dict(self):
        actual = self.pathwayservice._convert_domain_priority_dict(self.test["_6"]["input"])        
        self.assertEqual(actual, self.test["_6"]["expected_output"])

    def test_get_weakness_based_on_priority(self):
        self.pathwayservice.get_grade_domain_priorities = MagicMock(return_value=self.test["_7"]["input"][0])
        weakness = self.pathwayservice._resolveWeaknessBasedOnPriority(self.test["_7"]["input"][1])
        self.assertEqual(weakness[1], self.test["_7"]["expected_output"])

    def _prepare_max_priority_func_returns_valid_max_priority_function(self):
        self.pathwayservice.get_grade_domain_priorities = MagicMock(return_value=self.test["_8"]["input"][0])
        max_priority_func = self.pathwayservice._prepare_max_priority(self.test["_8"]["input"][1])
        self.assertEqual(max_priority_func(self.test["_8"]["input"][2],self.test["_8"]["input"][3]), self.test["_8"]["expected_output"])

    def test_fetch_learning(self):
        
        def side_effect(**kwargs): 
            return self.test["_9"]["input"][1][str(kwargs["grade_level"])]

        self.pathwayservice.get_all_score_cards = MagicMock( grade="1", return_value=self.test["_9"]["input"][0])
        self.pathwayservice.get_grade_domain_priorities = MagicMock( side_effect=side_effect)
        learning_path = self.pathwayservice.fetch_learning_path()
        self.assertListEqual(learning_path[1], self.test["_9"]["expected_output"][1])

if __name__ == "__main__":
    unittest.main() 
