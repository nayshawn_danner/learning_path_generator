#!/usr/bin/python
import unittest
from unittest.mock import MagicMock
from context import pathway_service
from pathway_service.domain.model import *

class GradeLevelTest(unittest.TestCase):
    def setUp(self):
        self.test = {"_1":{"input":"" ,"expected_output":GradeLevel("K")}}

    def test_set_grade_level_as_K_when_no_level_is_given(self):
        actual_student = GradeLevel(self.test["_1"]["input"])
        self.assertEqual(actual_student, self.test["_1"]["expected_output"])

class CCSDomainScoreTest(unittest.TestCase):
    def setUp(self):
        self.test = {   "_1":{
                                "input": CCSDomain(category_code="RF"),
                                "expected_output":CCSDomain(category_code="RF")
                        },
                        "_2":{
                                "input":GradeLevel("K"),
                                "expected_output":GradeLevel("K")
                        }
                    }

    def test_init(self):
        actual_domain_score = CCSDomainScore(domain=CCSDomain(category_code="RF"),grade_level=GradeLevel("K"))
        self.assertIsInstance(actual_domain_score, CCSDomainScore)
        pass

    def test_init_raises_on_invalid_domain_and_grade_level(self):
        self.assertRaises(Exception,
            lambda _: CCSDomainScore(domain="RF",grade_level="an_invalid_level"), None)

        self.assertRaises(Exception,
            lambda _: CCSDomainScore(domain="",grade_level=3), None)

        self.assertRaises(Exception,
            lambda _: CCSDomainScore(domain="RS",grade_level=None), None)

    def test_init_sets_domain(self):
        actual = CCSDomainScore(domain=self.test["_1"]["input"],grade_level=GradeLevel("K"))    
        self.assertEqual(actual.domain, self.test["_1"]["expected_output"])

    def test_init_sets_grade_level(self):
        actual = CCSDomainScore(domain=CCSDomain(category_code="RF"),grade_level=self.test["_2"]["input"])  
        self.assertEqual(actual.grade_level, self.test["_2"]["expected_output"])

if __name__ == "__main__":
    unittest.main() 
